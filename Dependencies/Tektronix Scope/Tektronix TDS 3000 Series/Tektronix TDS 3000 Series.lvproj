<?xml version='1.0'?>
<Project Type="Project" LVVersion="8008005">
   <Property Name="Instrument Driver" Type="Str">True</Property>
   <Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
   <Item Name="My Computer" Type="My Computer">
      <Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="Examples" Type="Folder">
         <Item Name="Tektronix TDS 3000 Series Auto Setup Waveform Acquire.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Examples/Tektronix TDS 3000 Series Auto Setup Waveform Acquire.vi"/>
         <Item Name="Tektronix TDS 3000 Series Dual channel Waveform Acquire.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Examples/Tektronix TDS 3000 Series Dual channel Waveform Acquire.vi"/>
         <Item Name="Tektronix TDS 3000 Series Single Channel Waveform Acquire.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Examples/Tektronix TDS 3000 Series Single Channel Waveform Acquire.vi"/>
         <Item Name="Tektronix TDS 3000 Series Transfer Current Waveform Example.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Examples/Tektronix TDS 3000 Series Transfer Current Waveform Example.vi"/>
         <Item Name="Tektronix TDS 3000 Series Wait For Acquisition Complete Example.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Examples/Tektronix TDS 3000 Series Wait For Acquisition Complete Example.vi"/>
         <Item Name="Tektronix TDS 3000 Series.bin3" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Examples/Tektronix TDS 3000 Series.bin3"/>
      </Item>
      <Item Name="Tektronix TDS 3000 Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Tektronix TDS 3000 Series.lvlib">
         <Item Name="Public" Type="Folder">
            <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            <Item Name="Action-Status" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Action-Status.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Actual Record Length.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Actual Record Length.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Actual Sample Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Actual Sample Mode.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Actual Sample Rate.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Actual Sample Rate.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Auto Probe Sense Value.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Auto Probe Sense Value.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Enable Math Channel.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Enable Math Channel.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Enable Reference Channel.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Enable Reference Channel.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Store Wfm To Reference Channel.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Action-Status/Store Wfm To Reference Channel.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="Configuration" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Acquisition" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configuration_Acquisition.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Acquisition/Configuration_Acquisition.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Acquisition Record.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Acquisition/Configure Acquisition Record.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Acquisition Type.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Acquisition/Configure Acquisition Type.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Initiate Continuous.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Acquisition/Configure Initiate Continuous.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Number of Averages.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Acquisition/Configure Number of Averages.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Number of Envelopes.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Acquisition/Configure Number of Envelopes.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Channel" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configuration_Channel.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Channel/Configuration_Channel.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Chan Characteristics.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Channel/Configure Chan Characteristics.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Channel.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Channel/Configure Channel.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Math Channels" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configuration_Math Channels.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Math Channels/Configuration_Math Channels.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Math Channel FFT.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Math Channels/Configure Math Channel FFT.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Math Channel.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Math Channels/Configure Math Channel.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Measurement" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configuration_Measurement.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Measurement/Configuration_Measurement.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Measurement.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Measurement/Configure Measurement.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Reference Levels.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Measurement/Configure Reference Levels.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Vertical Cursors.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Measurement/Configure Vertical Cursors.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Trigger" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Configuration_Trigger.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configuration_Trigger.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure AC Line Trigger Slope.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure AC Line Trigger Slope.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Edge Trigger Source.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Edge Trigger Source.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Glitch Trigger Source.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Glitch Trigger Source.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Logic Trigger Inputs.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Logic Trigger Inputs.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Pattern Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Pattern Trigger.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Runt Trigger Source.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Runt Trigger Source.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Slew Rate Trigger Src.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Slew Rate Trigger Src.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure State Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure State Trigger.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Trigger Coupling.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Trigger Coupling.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Trigger Modifier.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Trigger Modifier.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure Trigger.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure TV Trig HDTV Format.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure TV Trig HDTV Format.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure TV Trig Line Number.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure TV Trig Line Number.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure TV Trigger Source.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure TV Trigger Source.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Configure TV Trig Custom.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Trigger/Configure TV Trig Custom.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Configuration.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Configuration.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Auto Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Configuration/Auto Setup.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="Data" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Low Level" Type="Folder">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  <Item Name="Data_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Data_Low Level.mnu">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Abort.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Fetch Min Max Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Fetch Min Max Waveform.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Fetch Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Fetch Waveform.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Initiate Acquisition.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Initiate Acquisition.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Send Software Trigger.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
                  <Item Name="Wait for Acquisition Complete.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Low Level/Wait for Acquisition Complete.vi">
                     <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
                  </Item>
               </Item>
               <Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Data.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Cursors.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Read Cursors.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Min Max Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Read Min Max Waveform.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Waveform Measurement.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Read Waveform Measurement.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Read Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Data/Read Waveform.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="Utility" Type="Folder">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               <Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Utility/Utility.mnu">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Error Message.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Utility/Error Message.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Error Query (Multiple).vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Utility/Error Query (Multiple).vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Utility/Reset.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Utility/Revision Query.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
               <Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Utility/Self-Test.vi">
                  <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
               </Item>
            </Item>
            <Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/dir.mnu">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Close.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/Initialize.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Public/VI Tree.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
         </Item>
         <Item Name="Private" Type="Folder">
            <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            <Item Name="Clear.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Private/Clear.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="GetProperSampleRate.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Private/GetProperSampleRate.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="GetRawWaveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Private/GetRawWaveform.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
            <Item Name="Init Options.vi" Type="VI" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Private/Init Options.vi">
               <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
            </Item>
         </Item>
         <Item Name="Tektronix TDS 3000 Series Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Tektronix TDS 3000 Series/Tektronix TDS 3000 Series Readme.html">
            <Property Name="NI.Lib.ShowInTree" Type="Bool">true</Property>
         </Item>
      </Item>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
