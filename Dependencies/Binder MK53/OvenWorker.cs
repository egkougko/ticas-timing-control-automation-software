﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;
using MyLogHelper;

namespace OvenWorkerNamespace
{
    /// <summary>
    /// Binder Temerature oven worker
    /// </summary>
    internal class OvenWorker
    {
        private TcpClient oven = null;
        private Socket sock = null;
        private IPEndPoint serverEndPoint = null;
        private NetworkStream ovenStream = null;
        private BufferedStream bufferedOvenStream = null;

        private bool useSockets = true;
        private float currentTemp = 0;
        private string ipaddress = "";
        private int port = 0;

        private LogHelper logHelper = null;

        
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="loghelp"></param>
        internal OvenWorker(ref LogHelper loghelp)
        {
            logHelper = loghelp;
        }


        /// <summary>
        /// Sets params
        /// </summary>
        /// <param name="p_ipaddress"></param>
        /// <param name="p_port"></param>
        internal void SetParams(string p_ipaddress, int p_port)
        {
            this.ipaddress = p_ipaddress;
            this.port = p_port;
        }


        /// <summary>
        /// Starts connection
        /// </summary>
        /// <param name="ipaddress"></param>
        /// <param name="port"></param>
        private void StartConnection()
        {
            if (port == 0)
                throw new Exception("Erst IpAdresse und Port setzen");

            if (useSockets)
            {
                //logHelper.AddLog("Starting connection to oven over sockets. IpAddress: " + ipaddress + " Port:" + port + "...");
                sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverEndPoint = new IPEndPoint(IPAddress.Parse(ipaddress), port);
                sock.Connect(serverEndPoint);
                //logHelper.AddLog("OK. connection to oven established.");
            }
            else
            {
                oven = new TcpClient();
                serverEndPoint = new IPEndPoint(IPAddress.Parse(ipaddress), port);
                oven.Connect(serverEndPoint);
                ovenStream = oven.GetStream();
                bufferedOvenStream = new BufferedStream(ovenStream);
            }
        }


        /// <summary>
        /// Closes connection
        /// </summary>
        internal void CloseConnection()
        {
            if (useSockets)
            {
                sock.Disconnect(false);
                sock.Close();
                sock.Dispose();
                sock = null;
            }
            else
            {
                oven.Close();
                oven = null;
            }
        }


        /// <summary>
        /// Performs send and read operations
        /// </summary>
        /// <param name="data_to_send"></param>
        /// <param name="estimatedResponseLength"></param>
        /// <returns></returns>        
        private byte[] SendRead(byte[] data_to_send, int estimatedResponseLength)
        {
            int bytesRead = 0;
            int currIdx = 0;
            byte[] buffer = new byte[estimatedResponseLength];

            string msg = "";
            foreach(byte b in data_to_send)
            {
                msg += b.ToString("X") + " ";
            }
            logHelper?.AddLog("Sending " + msg);

            if (useSockets)
            {
                sock.Send(data_to_send);

                Thread.Sleep(100);

                if (sock.Available > 0)
                {
                    bytesRead += sock.Receive(buffer, currIdx, estimatedResponseLength, SocketFlags.None);

                    CheckOvenAnswer(buffer);
                    
                    return buffer;
                }
                else
                {
                    throw new Exception("No answer");
                }
            }
            else
            {
                bufferedOvenStream.Write(data_to_send, 0, data_to_send.Length);
                bufferedOvenStream.Flush();

                Thread.Sleep(100);

                // Blocks until a client sends a message                                   
                bytesRead = bufferedOvenStream.Read(buffer, currIdx, estimatedResponseLength);
                
                CheckOvenAnswer(buffer);
                
                return buffer;
            }
        }


        private void CheckOvenAnswer(byte[] buffer)
        {
            string msg = "";
            foreach (byte b in buffer)
            {
                msg += b.ToString("X") + " ";
            }
            logHelper?.AddLog("Received: " + msg);

            switch (buffer[2])
            {
                case 0: break;//OK 
                case 1: throw new Exception("Invalid function");
                case 2: throw new Exception("Ungültige Parameteradresse");
                case 3: throw new Exception("Parameter außerhalb des Wertebereiches");
                //case 4: throw new Exception("Temperaturkammer nicht bereit");
                case 5: throw new Exception("Diese Adresse ist schreibgeschützt");
                default: break;//throw new Exception("Unbekannter Fehler aufgetreten");
            }
        }


        /// <summary>
        /// Converts modbus bytes to float
        /// </summary>
        /// <param name="float_in_4_bytes"></param>
        /// <returns></returns>
        private float ConvertBytesToFloat(byte[] float_in_4_bytes)
        {
            byte[] temp2 = new byte[4];
            //rearrange
            temp2[0] = float_in_4_bytes[1];
            temp2[1] = float_in_4_bytes[0];
            temp2[2] = float_in_4_bytes[3];
            temp2[3] = float_in_4_bytes[2];
            //1,1,1,1 would give --> 2.369428E-38

            float f = System.BitConverter.ToSingle(temp2, 0);

            return f;
        }


        /// <summary>
        /// Converts float to bytes
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        byte[] ConvertFloatToBytes(float f)
        {
            byte[] bytes = System.BitConverter.GetBytes(f);
            byte[] res = new byte[4];

            //rearrange
            res[0] = bytes[1];
            res[1] = bytes[0];
            res[2] = bytes[3];
            res[3] = bytes[2];
            //20.32 soll ergeben: 0x41 0xA2 0f8F 0x5C

            return res;
        }


        /// <summary>
        /// Interpretes data as temperature
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private float InterpreteAsTemperature(byte[] data)
        {
            byte[] mod_bytes_temp = new byte[4];

            mod_bytes_temp[0] = data[3];
            mod_bytes_temp[1] = data[4];
            mod_bytes_temp[2] = data[5];
            mod_bytes_temp[3] = data[6];

            float curr_temp = ConvertBytesToFloat(mod_bytes_temp);
            return curr_temp;
        }


        /// <summary>
        /// Connects, reads temps and closes connection
        /// </summary>
        /// <param name="ipaddress"></param>
        /// <param name="port"></param>
        /// <param name="curr_temp"></param>
        internal void Connect_ReadTemp_Disconnect(out float curr_soll_temp, out float curr_ist_temp)
        {
            StartConnection();
            curr_ist_temp = ReadCurrIstTemp();
            curr_soll_temp = ReadCurrSollTemp();
            CloseConnection();
        }


        /// <summary>
        /// Connects, reads temp and humidity and closes connection
        /// </summary>
        /// <param name="ipaddress"></param>
        /// <param name="port"></param>
        /// <param name="curr_temp"></param>
        /// <param name="curr_humid"></param>
        internal void Connect_ReadTempHumid_Disconnect(out float curr_soll_temp, out float curr_ist_temp, out float curr_humid)
        {
            StartConnection();
            curr_soll_temp = ReadCurrSollTemp();
            curr_ist_temp = ReadCurrIstTemp();
            curr_humid = ReadCurrHumid();
            CloseConnection();
        }


        /// <summary>
        /// Reads current soll temperature
        /// </summary>
        /// <returns></returns>
        private float ReadCurrSollTemp()
        {
            List<byte> list = new List<byte>();
            list.Add(1); //chamber address
            list.Add(3); //3 or 4: read function
            UInt16 baseAddr = 0x1581; //manuelle soll-Temperatur
            list.Add((byte)((baseAddr >> 8) & 0xFF)); //high byte of addr
            list.Add((byte)(baseAddr & 0xFF)); //low byte of addr
            UInt16 countToRead = 2;
            list.Add((byte)((countToRead >> 8) & 0xFF)); //high byte of count
            list.Add((byte)(countToRead & 0xFF)); //low byte of count

            UInt16 crc = Crc16_Modbus.ComputeCrc_Modbus(list.ToArray());
            list.Add((byte)(crc & 0xFF)); //low byte of crc
            list.Add((byte)((crc >> 8) & 0xFF)); //high byte of crc

            int estimatedResponseLength = countToRead * 2 + 5;

            byte[] rec = SendRead(list.ToArray(), estimatedResponseLength);

            this.currentTemp = InterpreteAsTemperature(rec);

            return currentTemp;
        }


        /// <summary>
        /// Reads current ist temperature
        /// </summary>
        /// <returns></returns>
        private float ReadCurrIstTemp()
        {
            List<byte> list = new List<byte>();
            list.Add(1); //chamber address
            list.Add(3); //3 or 4: read function
            UInt16 baseAddr = 0x11A9;
            list.Add((byte)((baseAddr >> 8) & 0xFF)); //high byte of addr
            list.Add((byte)(baseAddr & 0xFF)); //low byte of addr
            UInt16 countToRead = 2;
            list.Add((byte)((countToRead >> 8) & 0xFF)); //high byte of count
            list.Add((byte)(countToRead & 0xFF)); //low byte of count

            UInt16 crc = Crc16_Modbus.ComputeCrc_Modbus(list.ToArray());
            list.Add((byte)(crc & 0xFF)); //low byte of crc
            list.Add((byte)((crc >> 8) & 0xFF)); //high byte of crc

            int estimatedResponseLength = countToRead * 2 + 5;

            byte[] rec = SendRead(list.ToArray(), estimatedResponseLength);

            this.currentTemp = InterpreteAsTemperature(rec);

            return currentTemp;
        }


        /// <summary>
        /// Reads current humidity
        /// </summary>
        /// <returns></returns>
        private float ReadCurrHumid()
        {
            List<byte> list = new List<byte>();
            list.Add(1); //chamber address
            list.Add(3); //3 or 4: read function
            UInt16 baseAddr = 0x11CD; //curr humidity
            list.Add((byte)((baseAddr >> 8) & 0xFF)); //high byte of addr
            list.Add((byte)(baseAddr & 0xFF)); //low byte of addr
            UInt16 countToRead = 2;
            list.Add((byte)((countToRead >> 8) & 0xFF)); //high byte of count
            list.Add((byte)(countToRead & 0xFF)); //low byte of count

            UInt16 crc = Crc16_Modbus.ComputeCrc_Modbus(list.ToArray());
            list.Add((byte)(crc & 0xFF)); //low byte of crc
            list.Add((byte)((crc >> 8) & 0xFF)); //high byte of crc

            int estimatedResponseLength = countToRead * 2 + 5;

            byte[] rec = SendRead(list.ToArray(), estimatedResponseLength);

            this.currentTemp = InterpreteAsTemperature(rec);

            return currentTemp;
        }


                      
        /// <summary>
        /// Connects, sets new temperatur, disconnects
        /// </summary>
        /// <param name="new_temp"></param>
        internal void Connect_SetNewTemp_Disconnect(float new_temp)
        {
            StartConnection();
            UInt16 addr = 0x1581; //set point 1 manual
            SetTemp(new_temp, addr);
            CloseConnection();
        }


        private void Connect_SetNewTemp_Disconnect(float new_temp, UInt16 baseAddr)
        {
            StartConnection();
            SetTemp(new_temp, baseAddr);
            CloseConnection();
        }


        /// <summary>
        /// Sets new temperature
        /// </summary>
        /// <param name="new_temp"></param>
        /// <param name="baseAddr"></param>
        private void SetTemp(float new_temp, UInt16 baseAddr)
        {
            //SA FC AD AD NW NB D .. D CRC16
            //SA: Slave address
            //FC: Function code
            //AD: ADDRESS
            //NW: Number of words
            //NB: Number of bytes
            //D : DATA
            //CRC16: 2 byte CRC16
            List<byte> list = new List<byte>();
            list.Add(1); //chamber address
            list.Add(0x10); //6:write one word, 0x10:write max 80 words
            //UInt16 baseAddr = 0x09F8; // 0x1A69; //set point 1 Temperature R/W

            list.Add((byte)((baseAddr >> 8) & 0xFF)); //high byte of addr
            list.Add((byte)(baseAddr & 0xFF)); //low byte of addr

            UInt16 wordsToWrite = 2; //write 2 words = 4 bytes
            list.Add((byte)((wordsToWrite >> 8) & 0xFF)); //high byte of count
            list.Add((byte)(wordsToWrite & 0xFF)); //low byte of count
            
            list.Add((byte)(wordsToWrite * 2)); //number of bytes to write

            float newTemp = new_temp;//currentTemp + inc_temp; 
            byte[] data = ConvertFloatToBytes(newTemp);
            list.AddRange(data); //add data

            UInt16 crc = Crc16_Modbus.ComputeCrc_Modbus(list.ToArray());
            list.Add((byte)(crc & 0xFF)); //low byte of crc
            list.Add((byte)((crc >> 8) & 0xFF)); //high byte of crc

            int estimatedResponseLength = 8; //always 8 bytes as response estimated

            byte[] rec = SendRead(list.ToArray(), estimatedResponseLength);
        }        
    }


    /// <summary>
    /// CRC for modbus generation class
    /// </summary>
    public class Crc16_Modbus
    {
        private static ushort[] CrcTable = {
        0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
        0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
        0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
        0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
        0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
        0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
        0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
        0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
        0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
        0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
        0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
        0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
        0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
        0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
        0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
        0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
        0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
        0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
        0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
        0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
        0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
        0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
        0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
        0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
        0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
        0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
        0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
        0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
        0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
        0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
        0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
        0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

        /// <summary>
        /// Calculates CRC (Modbus)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static UInt16 ComputeCrc_Modbus(byte[] data)
        {
            ushort crc = 0xFFFF;

            foreach (byte datum in data)
            {
                crc = (ushort)((crc >> 8) ^ CrcTable[(crc ^ datum) & 0xFF]);
            }

            return crc;
        }
    }
}
