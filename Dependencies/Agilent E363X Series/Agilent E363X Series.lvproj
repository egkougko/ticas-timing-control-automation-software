﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS_hidden,Win;CPU_hidden,x86;OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Agilent E363X Series.bin3" Type="Document" URL="/&lt;instrlib&gt;/Agilent E363X Series/Examples/Agilent E363X Series.bin3"/>
			<Item Name="Agilent E363X Series Output DC Volts.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent E363X Series/Examples/Agilent E363X Series Output DC Volts.vi"/>
			<Item Name="Agilent E363X Series Output DC Volts - OVP OCP.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent E363X Series/Examples/Agilent E363X Series Output DC Volts - OVP OCP.vi"/>
			<Item Name="Agilent E363X Series Output DC Volts - Triggered.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent E363X Series/Examples/Agilent E363X Series Output DC Volts - Triggered.vi"/>
		</Item>
		<Item Name="Agilent E363X Series.lvlib" Type="Library" URL="../Agilent E363X Series.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Select Event Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Select Event Type.ctl"/>
				<Item Name="Wait for RQS.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Wait for RQS.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
