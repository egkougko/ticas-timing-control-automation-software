Votsch Oven Control VI Library ver2.0
=====================================

This library has been developed specifically to control two models of Votsch
oven, a small VT4002 desk oven, and a VT3050 floor-standing oven.  These are
the two that  the library  has been  developed  with and  will be  used with
initially.  These ovens are fitted with temperature  control only,  not full
climatic capabilities.

The underlying principles and VIs should work  with other  Votsch ovens with
the same  controller,  including those  with greater functionality. However,
do  not expect  these VIs to  provide full  functionality as  they currently
stand.

Full source code is provided,  plus  a sample data string from the two ovens
used for development.

In order for the RS232 communications to work the oven needs to be set up to
communicate using the port. The settings required on the oven itself were:
	OP0 = 3
	OP2 = 0
	OP3 = 0
	OP4 = 1

The serial port should be set to 9600baud,8,n,1 - no handshaking.