﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Agilent 8000 Series Acquire Continuous Digital Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Acquire Continuous Digital Waveform.vi"/>
			<Item Name="Agilent 8000 Series Acquire Continuous Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Acquire Continuous Waveform.vi"/>
			<Item Name="Agilent 8000 Series Acquire Math Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Acquire Math Waveform.vi"/>
			<Item Name="Agilent 8000 Series Acquire Multiple Waveforms.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Acquire Multiple Waveforms.vi"/>
			<Item Name="Agilent 8000 Series Acquire Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Acquire Waveform.vi"/>
			<Item Name="Agilent 8000 Series Edge Triggered Acquisition.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Edge Triggered Acquisition.vi"/>
			<Item Name="Agilent 8000 Series Histogram Measurement.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Histogram Measurement.vi"/>
			<Item Name="Agilent 8000 Series Mask Test.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Mask Test.vi"/>
			<Item Name="Agilent 8000 Series Transfer Current Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series Transfer Current Waveform.vi"/>
			<Item Name="Agilent 8000 Series.bin3" Type="Document" URL="/&lt;instrlib&gt;/Agilent 8000 Series/Examples/Agilent 8000 Series.bin3"/>
		</Item>
		<Item Name="Agilent 8000 Series.lvlib" Type="Library" URL="../Agilent 8000 Series.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Binary to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Binary to Digital.vi"/>
				<Item Name="DWDT Binary U32 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Binary U32 to Digital.vi"/>
				<Item Name="DTbl Binary U32 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Binary U32 to Digital.vi"/>
				<Item Name="Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Compress Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DWDT Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Compress Digital.vi"/>
				<Item Name="DWDT Binary U16 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Binary U16 to Digital.vi"/>
				<Item Name="DTbl Binary U16 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Binary U16 to Digital.vi"/>
				<Item Name="DWDT Binary U8 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Binary U8 to Digital.vi"/>
				<Item Name="DTbl Binary U8 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Binary U8 to Digital.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
