﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="UFSD HMP4040.vi" Type="VI" URL="../UFSD HMP4040.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="_hmp4000 Coercing Warning.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/_utility/_hmp4000 Coercing Warning.vi"/>
				<Item Name="_hmp4000 Default Instrument Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/_utility/_hmp4000 Default Instrument Setup.vi"/>
				<Item Name="hmp4000 Clear Over Voltage Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/Voltage Configuration/hmp4000 Clear Over Voltage Protection.vi"/>
				<Item Name="hmp4000 Close.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/hmp4000 Close.vi"/>
				<Item Name="hmp4000 Configure Channel.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/hmp4000 Configure Channel.vi"/>
				<Item Name="hmp4000 Configure Current Value.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/Current Configuration/hmp4000 Configure Current Value.vi"/>
				<Item Name="hmp4000 Configure General Output State.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/hmp4000 Configure General Output State.vi"/>
				<Item Name="hmp4000 Configure Output State Channel Only.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/hmp4000 Configure Output State Channel Only.vi"/>
				<Item Name="hmp4000 Configure Over Voltage Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/Voltage Configuration/hmp4000 Configure Over Voltage Protection.vi"/>
				<Item Name="hmp4000 Configure Voltage Value.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/Voltage Configuration/hmp4000 Configure Voltage Value.vi"/>
				<Item Name="hmp4000 Error Message.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/hmp4000 Error Message.vi"/>
				<Item Name="hmp4000 ID Query Response.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/hmp4000 ID Query Response.vi"/>
				<Item Name="hmp4000 Initialize with Options.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/hmp4000 Initialize with Options.vi"/>
				<Item Name="hmp4000 Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/hmp4000 Initialize.vi"/>
				<Item Name="hmp4000 Query Over Voltage Protection Status.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Configuration/Voltage Configuration/hmp4000 Query Over Voltage Protection Status.vi"/>
				<Item Name="hmp4000 Query ViString.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/Instrument IO/hmp4000 Query ViString.vi"/>
				<Item Name="hmp4000 Read Current Output.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Measurement/hmp4000 Read Current Output.vi"/>
				<Item Name="hmp4000 Read Voltage Output.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Measurement/hmp4000 Read Voltage Output.vi"/>
				<Item Name="hmp4000 Remote Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/hmp4000 Remote Mode.vi"/>
				<Item Name="hmp4000 Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/hmp4000 Reset.vi"/>
				<Item Name="hmp4000 Set OPC Timeout.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/hmp4000 Set OPC Timeout.vi"/>
				<Item Name="hmp4000 Write Command.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/Utility/Instrument IO/hmp4000 Write Command.vi"/>
				<Item Name="hmp4000_check_error.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/_utility/hmp4000_check_error.vi"/>
				<Item Name="hmp4000_core_global.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/_utility/hmp4000_core_global.vi"/>
				<Item Name="hmp4000_Delayed_Int32_WriteCallback.vi" Type="VI" URL="/&lt;instrlib&gt;/hmp4000/_utility/callbacks/hmp4000_Delayed_Int32_WriteCallback.vi"/>
				<Item Name="hmp4000_rsidr_core.lvlib" Type="Library" URL="/&lt;instrlib&gt;/hmp4000/_utility/rsidr_core.llb/hmp4000_rsidr_core.lvlib"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="subhmp4000_core_attribute_express.vi" Type="VI" URL="/&lt;userlib&gt;/_express/hmp4000/hmp4000_core_attribute_expressSource.llb/subhmp4000_core_attribute_express.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
			</Item>
			<Item Name="CompressedAir On-Off LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/CompressedAir On-Off LAN.vi"/>
			<Item Name="Configure Compliance.vi" Type="VI" URL="../../dependencies/Keithley 24XX/Public/Configure/Configure Compliance.vi"/>
			<Item Name="Get Parameters LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Get Parameters LAN.vi"/>
			<Item Name="Keithley 24XX.lvlib" Type="Library" URL="../../dependencies/Keithley 24XX/Keithley 24XX.lvlib"/>
			<Item Name="Oven On-Off LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Oven On-Off LAN.vi"/>
			<Item Name="Oven SetPoint LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Oven SetPoint LAN.vi"/>
			<Item Name="Send-Receive Command LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Send-Receive Command LAN.vi"/>
			<Item Name="Set parameters LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Set parameters LAN.vi"/>
			<Item Name="View setpoint,temp and on-off LAN.vi" Type="VI" URL="../../dependencies/Votsch VT4002/Votsch Oven Control v2.llb/View setpoint,temp and on-off LAN.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="UFSD HMP4040" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{17C586C4-6839-48A4-91DF-EAEA67F35DFC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{8F6BD51A-CE6E-48B1-AB43-A9F7A9B872AD}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.UCSC.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F7BDB25D-7ECF-4BFF-9143-6FD3BAA7A00A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">UFSD HMP4040</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{FF0FD68B-6F8A-4B96-AE3D-303EEE9C327A}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">UFSD HMP4040.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/UFSD HMP4040.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{569444E3-E5C6-4457-A1F8-463BBAD251E5}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UFSD HMP4040.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">IFAE</Property>
				<Property Name="TgtF_fileDescription" Type="Str">UFSD HMP4040</Property>
				<Property Name="TgtF_internalName" Type="Str">UFSD HMP4040</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 Vagelis Gkougkousis</Property>
				<Property Name="TgtF_productName" Type="Str">UFSD HMP4040</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{7B74FDA1-9C3B-4E14-9468-09843410BE19}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">UFSD HMP4040.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
