﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="UFSD Control.vi" Type="VI" URL="../UFSD Control.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Binary to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Binary to Digital.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Bytes At Serial Port.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Bytes At Serial Port.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Serial Driver.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Close Serial Driver.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatWriteText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatWriteText.vi"/>
				<Item Name="Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Compress Digital.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="DTbl Binary U8 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Binary U8 to Digital.vi"/>
				<Item Name="DTbl Binary U16 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Binary U16 to Digital.vi"/>
				<Item Name="DTbl Binary U32 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Binary U32 to Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DWDT Binary U8 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Binary U8 to Digital.vi"/>
				<Item Name="DWDT Binary U16 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Binary U16 to Digital.vi"/>
				<Item Name="DWDT Binary U32 to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Binary U32 to Digital.vi"/>
				<Item Name="DWDT Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Compress Digital.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVAllMonitorsDisplayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVAllMonitorsDisplayTypeDef.ctl"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open Serial Driver.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_sersup.llb/Open Serial Driver.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Select Event Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Select Event Type.ctl"/>
				<Item Name="Serial Port Init.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Init.vi"/>
				<Item Name="Serial Port Read.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Read.vi"/>
				<Item Name="Serial Port Write.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Write.vi"/>
				<Item Name="serpConfig.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/serpConfig.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Wait for RQS.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Wait for RQS.vi"/>
				<Item Name="Waveform Scale and Offset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Waveform Scale and Offset.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ (string).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
			</Item>
			<Item Name="2d_Array_sortSimplified.vi" Type="VI" URL="../2d_Array_sortSimplified.vi"/>
			<Item Name="_hmp4000 Coercing Warning.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/_utility/_hmp4000 Coercing Warning.vi"/>
			<Item Name="_hmp4000 Default Instrument Setup.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/_utility/_hmp4000 Default Instrument Setup.vi"/>
			<Item Name="Agilent 363x RefeNum.vi" Type="VI" URL="../../Agilent 363x power supply/Agilent 363x RefeNum.vi"/>
			<Item Name="Agilent 364x RefeNum.vi" Type="VI" URL="../../Agilent 364x power supply/Agilent 364x RefeNum.vi"/>
			<Item Name="Agilent 8000 Series.lvlib" Type="Library" URL="../../../Dependencies/Agilent 8000 Series/Agilent 8000 Series.lvlib"/>
			<Item Name="Agilent E363X Series.lvlib" Type="Library" URL="../../../Dependencies/Agilent E363X Series/Agilent E363X Series.lvlib"/>
			<Item Name="Agilent E364X Series.lvlib" Type="Library" URL="../../../Dependencies/Agilent E364X Series/Agilent E364X Series.lvlib"/>
			<Item Name="Autotrigger Oscilloscope config.vi" Type="VI" URL="../../Oscilloscopes/Autotrigger Oscilloscope config.vi"/>
			<Item Name="Autotrigger Voltage Point.vi" Type="VI" URL="../Autotrigger/Autotrigger Voltage Point.vi"/>
			<Item Name="Autotrigger Voltage Set.vi" Type="VI" URL="../Autotrigger/Autotrigger Voltage Set.vi"/>
			<Item Name="Autrotrigger.vi" Type="VI" URL="../Autotrigger/Autrotrigger.vi"/>
			<Item Name="Baysian_Uncertenties.vi" Type="VI" URL="../Baysian_Uncertenties.vi"/>
			<Item Name="binder_MKT115_Check_Data.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Check_Data.vi"/>
			<Item Name="binder_MKT115_CRC16.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_CRC16.vi"/>
			<Item Name="binder_MKT115_Get_AllSetHumidities.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Get_AllSetHumidities.vi"/>
			<Item Name="binder_MKT115_Get_AllSetTemp.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Get_AllSetTemp.vi"/>
			<Item Name="binder_MKT115_Get_DryAir.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Get_DryAir.vi"/>
			<Item Name="binder_MKT115_Get_Humidity.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Get_Humidity.vi"/>
			<Item Name="binder_MKT115_Get_ModeManual.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Get_ModeManual.vi"/>
			<Item Name="binder_MKT115_Get_Temp.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Get_Temp.vi"/>
			<Item Name="binder_MKT115_IEEE-MODbus.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_IEEE-MODbus.vi"/>
			<Item Name="binder_MKT115_Make_Data.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Make_Data.vi"/>
			<Item Name="binder_MKT115_Send_Command.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Send_Command.vi"/>
			<Item Name="binder_MKT115_Set_AllHumidities.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Set_AllHumidities.vi"/>
			<Item Name="binder_MKT115_Set_AllTemp.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Set_AllTemp.vi"/>
			<Item Name="binder_MKT115_Set_DryAir.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Set_DryAir.vi"/>
			<Item Name="binder_MKT115_Set_Humidity.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Set_Humidity.vi"/>
			<Item Name="binder_MKT115_Set_ModeManual.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Set_ModeManual.vi"/>
			<Item Name="binder_MKT115_Set_Temp.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/binder_MKT115_Set_Temp.vi"/>
			<Item Name="Close TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Close TTi-PL.vi"/>
			<Item Name="Cofigure Attenuation.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Cofigure Attenuation.vi"/>
			<Item Name="Cofigure Vertical Offset.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Cofigure Vertical Offset.vi"/>
			<Item Name="Cofigure Vertical Range.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Cofigure Vertical Range.vi"/>
			<Item Name="CompressedAir On-Off LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/CompressedAir On-Off LAN.vi"/>
			<Item Name="Configure Acquisition Mode.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Acquisition Mode.vi"/>
			<Item Name="Configure Attenuation Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Attenuation Tek.vi"/>
			<Item Name="Configure Bandwidth Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Bandwidth Tek.vi"/>
			<Item Name="Configure Bandwidth.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure Bandwidth.vi"/>
			<Item Name="Configure Channel Attenuation Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Channel Attenuation Agilent.vi"/>
			<Item Name="Configure Channel Bandwidth Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Channel Bandwidth Agilent.vi"/>
			<Item Name="Configure Channel ON-OFF Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Channel ON-OFF Agilent.vi"/>
			<Item Name="Configure Channel Vertical Scale Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Channel Vertical Scale Agilent.vi"/>
			<Item Name="Configure Channel Vertival Coupling Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Channel Vertival Coupling Agilent.vi"/>
			<Item Name="Configure Channel Vertival Offset Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Channel Vertival Offset Agilent.vi"/>
			<Item Name="Configure Combine Channels.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure Combine Channels.vi"/>
			<Item Name="Configure Compliance.vi" Type="VI" URL="../../../Dependencies/Keithley 24XX/Public/Configure/Configure Compliance.vi"/>
			<Item Name="Configure Coupling Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Coupling Tek.vi"/>
			<Item Name="Configure Coupling.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Private/Configure Coupling.vi"/>
			<Item Name="Configure Coupling.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure Coupling.vi"/>
			<Item Name="Configure Delay Mode.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Delay Mode.vi"/>
			<Item Name="Configure Display Grid.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure Display Grid.vi"/>
			<Item Name="Configure Edge Triger Coupling Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Edge Triger Coupling Tek.vi"/>
			<Item Name="Configure Edge Triger Slope Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Edge Triger Slope Tek.vi"/>
			<Item Name="Configure Edge Triger Source Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Edge Triger Source Tek.vi"/>
			<Item Name="Configure Edge Trigger Threshold Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Edge Trigger Threshold Tek.vi"/>
			<Item Name="Configure Fast Frame.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Fast Frame.vi"/>
			<Item Name="Configure Impedence Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Impedence Tek.vi"/>
			<Item Name="Configure Logic Pattern Type Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Logic Pattern Type Tek.vi"/>
			<Item Name="Configure Logic Trigg Condition Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Logic Trigg Condition Tek.vi"/>
			<Item Name="Configure Logic Trigger State Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Logic Trigger State Tek.vi"/>
			<Item Name="Configure Logic Trigger Threshold Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Logic Trigger Threshold Tek.vi"/>
			<Item Name="Configure ON-OFF Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure ON-OFF Tek.vi"/>
			<Item Name="Configure Output on-off.vi" Type="VI" URL="../../../Dependencies/Agilent E363X Series/Public/Configure/Configure Output on-off.vi"/>
			<Item Name="Configure Output Voltage.vi" Type="VI" URL="../../../Dependencies/Agilent E363X Series/Public/Configure/Configure Output Voltage.vi"/>
			<Item Name="Configure RealTimeMemory.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure RealTimeMemory.vi"/>
			<Item Name="Configure Sampling Rate Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Sampling Rate Agilent.vi"/>
			<Item Name="Configure SamplingRate LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure SamplingRate LeCroy.vi"/>
			<Item Name="Configure Segmented TTAG Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Segmented TTAG Agilent.vi"/>
			<Item Name="Configure Source Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Source Agilent.vi"/>
			<Item Name="Configure Streming.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Streming.vi"/>
			<Item Name="Configure Time Reference.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Configure Time Reference.vi"/>
			<Item Name="Configure Trigg Class Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Trigg Class Tek.vi"/>
			<Item Name="Configure Trigg Type Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Configure Trigg Type Tek.vi"/>
			<Item Name="Configure Trigger (Edge).vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Configure Trigger (Edge).vi"/>
			<Item Name="Configure Trigger Mode.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Trigger Mode.vi"/>
			<Item Name="Configure TriggerMode LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Configure TriggerMode LeCroy.vi"/>
			<Item Name="Configure Vertical Offset Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Vertical Offset Tek.vi"/>
			<Item Name="Configure Vertical Range Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure Vertical Range Tek.vi"/>
			<Item Name="Configure WaveFormFormat.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Configure Simple/Configure WaveFormFormat.vi"/>
			<Item Name="Control application Reference.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Control application Reference.vi"/>
			<Item Name="DataType.ctl" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/DataType.ctl"/>
			<Item Name="Error Management Autotrig.vi" Type="VI" URL="../Autotrigger/Error Management Autotrig.vi"/>
			<Item Name="Error Management.vi" Type="VI" URL="../Charged Particle/Error Management.vi"/>
			<Item Name="Error Message TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Utility VI&apos;s/Error Message TTi-PL.vi"/>
			<Item Name="Error Query (multiple) TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Utility VI&apos;s/Error Query (multiple) TTi-PL.vi"/>
			<Item Name="ErrorCheck.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/ErrorCheck.vi"/>
			<Item Name="Fetch (Waveform) to File.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Data/Low Level/Fetch (Waveform) to File.vi"/>
			<Item Name="Fetch Waveform Simple.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Data/Low Level/Fetch Waveform Simple.vi"/>
			<Item Name="Find Decoration by UID Tab.vi" Type="VI" URL="../../../Decorations Utilities/Find Decoration by UID Tab.vi"/>
			<Item Name="Fnid Resolution.vi" Type="VI" URL="../Fnid Resolution.vi"/>
			<Item Name="Get Comparator.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Get Comparator.vi"/>
			<Item Name="Get Mode.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Get Mode.vi"/>
			<Item Name="Get On-Off.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Get On-Off.vi"/>
			<Item Name="Get Parameters LAN NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/Get Parameters LAN NoAir.vi"/>
			<Item Name="Get Parameters LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Get Parameters LAN.vi"/>
			<Item Name="Get parameters Serial.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/Get parameters Serial.vi"/>
			<Item Name="Get SetPoint.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Get SetPoint.vi"/>
			<Item Name="Get Temperature.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Get Temperature.vi"/>
			<Item Name="hmp4000 Clear Over Voltage Protection.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/Voltage Configuration/hmp4000 Clear Over Voltage Protection.vi"/>
			<Item Name="hmp4000 Close.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/hmp4000 Close.vi"/>
			<Item Name="hmp4000 Configure Channel.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/hmp4000 Configure Channel.vi"/>
			<Item Name="hmp4000 Configure Current Value.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/Current Configuration/hmp4000 Configure Current Value.vi"/>
			<Item Name="hmp4000 Configure General Output State.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/hmp4000 Configure General Output State.vi"/>
			<Item Name="hmp4000 Configure Output State.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/hmp4000 Configure Output State.vi"/>
			<Item Name="hmp4000 Configure Over Voltage Protection.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/Voltage Configuration/hmp4000 Configure Over Voltage Protection.vi"/>
			<Item Name="hmp4000 Configure Voltage Value.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/Voltage Configuration/hmp4000 Configure Voltage Value.vi"/>
			<Item Name="hmp4000 Error Query.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/hmp4000 Error Query.vi"/>
			<Item Name="hmp4000 Get Output State.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Measurement/hmp4000 Get Output State.vi"/>
			<Item Name="hmp4000 ID Query Response.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/hmp4000 ID Query Response.vi"/>
			<Item Name="hmp4000 Initialize with Options.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/hmp4000 Initialize with Options.vi"/>
			<Item Name="hmp4000 Initialize.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/hmp4000 Initialize.vi"/>
			<Item Name="hmp4000 Query Combined Voltage And Current.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/hmp4000 Query Combined Voltage And Current.vi"/>
			<Item Name="hmp4000 Query Over Voltage Protection Status.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/Voltage Configuration/hmp4000 Query Over Voltage Protection Status.vi"/>
			<Item Name="hmp4000 Query Over Voltage Value.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Configuration/Voltage Configuration/hmp4000 Query Over Voltage Value.vi"/>
			<Item Name="hmp4000 Query ViString.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/Instrument IO/hmp4000 Query ViString.vi"/>
			<Item Name="hmp4000 Read Current Output.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Measurement/hmp4000 Read Current Output.vi"/>
			<Item Name="hmp4000 Read Voltage Output.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Measurement/hmp4000 Read Voltage Output.vi"/>
			<Item Name="hmp4000 Remote Mode.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/hmp4000 Remote Mode.vi"/>
			<Item Name="hmp4000 Reset.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/hmp4000 Reset.vi"/>
			<Item Name="hmp4000 Set OPC Timeout.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/hmp4000 Set OPC Timeout.vi"/>
			<Item Name="hmp4000 Write Command.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/Utility/Instrument IO/hmp4000 Write Command.vi"/>
			<Item Name="hmp4000_check_error.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/_utility/hmp4000_check_error.vi"/>
			<Item Name="hmp4000_core_global.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/_utility/hmp4000_core_global.vi"/>
			<Item Name="hmp4000_Delayed_Int32_WriteCallback.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/_utility/callbacks/hmp4000_Delayed_Int32_WriteCallback.vi"/>
			<Item Name="hmp4000_rsidr_core.lvlib" Type="Library" URL="../../../Dependencies/Hameg HMP4040/instr.lib/hmp4000/_utility/rsidr_core.llb/hmp4000_rsidr_core.lvlib"/>
			<Item Name="HMP4040 RefeNum.vi" Type="VI" URL="../../Hameg HMP4040/HMP4040 RefeNum.vi"/>
			<Item Name="IEEE2MOD.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/IEEE2MOD.vi"/>
			<Item Name="Initialize TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Initialize TTi-PL.vi"/>
			<Item Name="Initialize.vi" Type="VI" URL="../../../Dependencies/Agilent E364X Series/Public/Initialize.vi"/>
			<Item Name="Initiate RealTime.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Data/Low Level/Initiate RealTime.vi"/>
			<Item Name="Interface.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Data/Interface.vi"/>
			<Item Name="Inverse Unit Conversion.vi" Type="VI" URL="../../Hameg HMP4040/Inverse Unit Conversion.vi"/>
			<Item Name="Iterative Re-rounding.vi" Type="VI" URL="../Autotrigger/Iterative Re-rounding.vi"/>
			<Item Name="Keithley 24XX.lvlib" Type="Library" URL="../../../Dependencies/Keithley 24XX/Keithley 24XX.lvlib"/>
			<Item Name="Latch Action.vi" Type="VI" URL="../Latch Action.vi"/>
			<Item Name="LeCroy Wave Series.lvlib" Type="Library" URL="../../../Dependencies/LeCroy Wave Series/LeCroy Wave Series.lvlib"/>
			<Item Name="Level Units.vi" Type="VI" URL="../../Oscilloscopes/Level Units.vi"/>
			<Item Name="LV Decorations Tab.vi" Type="VI" URL="../LV Decorations Tab.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="MakeHeader Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Data/MakeHeader Agilent.vi"/>
			<Item Name="MakeHeader Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Data/MakeHeader Tek.vi"/>
			<Item Name="Measure TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Data VI&apos;s/Measure TTi-PL.vi"/>
			<Item Name="MOD2IEEE.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MOD2IEEE.vi"/>
			<Item Name="MODBuildCRC16.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODBuildCRC16.vi"/>
			<Item Name="MODBuildTelegramm.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODBuildTelegramm.vi"/>
			<Item Name="MODCheckResponse.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODCheckResponse.vi"/>
			<Item Name="MODErrorReport.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODErrorReport.vi"/>
			<Item Name="MODInterfaceWait.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODInterfaceWait.vi"/>
			<Item Name="MODReadDirect.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODReadDirect.vi"/>
			<Item Name="MODReadWrite.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODReadWrite.vi"/>
			<Item Name="MODWriteDirect.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/MODWriteDirect.vi"/>
			<Item Name="Monitor Event Aquisition File.vi" Type="VI" URL="../Charged Particle/Monitor Event Aquisition File.vi"/>
			<Item Name="Multiple Waveform Aquisition.vi" Type="VI" URL="../Charged Particle/Multiple Waveform Aquisition.vi"/>
			<Item Name="OperationMode.ctl" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/OperationMode.ctl"/>
			<Item Name="Oven On-Off LAN NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/Oven On-Off LAN NoAir.vi"/>
			<Item Name="Oven On-Off LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Oven On-Off LAN.vi"/>
			<Item Name="Oven On-Off Serial NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/Oven On-Off Serial NoAir.vi"/>
			<Item Name="Oven SetPoint LAN NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/Oven SetPoint LAN NoAir.vi"/>
			<Item Name="Oven SetPoint LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Oven SetPoint LAN.vi"/>
			<Item Name="Oven SetPoint Serial NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/Oven SetPoint Serial NoAir.vi"/>
			<Item Name="ProfileStatus.ctl" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/ProfileStatus.ctl"/>
			<Item Name="Query Active Channels Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Active Channels Agilent.vi"/>
			<Item Name="Query Active Channels LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Active Channels LeCroy.vi"/>
			<Item Name="Query Active Channels Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Active Channels Tek.vi"/>
			<Item Name="Query Attenuation LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Attenuation LeCroy.vi"/>
			<Item Name="Query Attenuation Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Attenuation Tek.vi"/>
			<Item Name="Query Bandwidth LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Bandwidth LeCroy.vi"/>
			<Item Name="Query Bandwidth Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Bandwidth Tek.vi"/>
			<Item Name="Query Channel Attenuation Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Channel Attenuation Agilent.vi"/>
			<Item Name="Query Channel Bandwidth Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Channel Bandwidth Agilent.vi"/>
			<Item Name="Query Channel ON-OFF Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Channel ON-OFF Agilent.vi"/>
			<Item Name="Query Channel Vertical Scale Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Channel Vertical Scale Agilent.vi"/>
			<Item Name="Query Channel Vertival Coupling Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Channel Vertival Coupling Agilent.vi"/>
			<Item Name="Query Channel Vertival Offset Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Channel Vertival Offset Agilent.vi"/>
			<Item Name="Query Coupling LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Coupling LeCroy.vi"/>
			<Item Name="Query Coupling Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Coupling Tek.vi"/>
			<Item Name="Query Current Limit TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Query VI&apos;s/Query Current Limit TTi-PL.vi"/>
			<Item Name="Query Current Limit.vi" Type="VI" URL="../../../Dependencies/Agilent E364X Series/Public/Action-Status VI&apos;s/Query Current Limit.vi"/>
			<Item Name="Query Delay Mode.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Delay Mode.vi"/>
			<Item Name="Query Edge Trigger Coupling Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Edge Trigger Coupling Tek.vi"/>
			<Item Name="Query Edge Trigger Slope Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Edge Trigger Slope Tek.vi"/>
			<Item Name="Query Edge Trigger Source Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Edge Trigger Source Tek.vi"/>
			<Item Name="Query Edge Trigger Threshold Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Edge Trigger Threshold Tek.vi"/>
			<Item Name="Query Error Registers TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Utility VI&apos;s/Query Error Registers TTi-PL.vi"/>
			<Item Name="Query Impedence Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Impedence Tek.vi"/>
			<Item Name="Query Limit Error TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Query VI&apos;s/Query Limit Error TTi-PL.vi"/>
			<Item Name="Query Logic Pattern Type Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Logic Pattern Type Tek.vi"/>
			<Item Name="Query Logic Trigg Condition Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Logic Trigg Condition Tek.vi"/>
			<Item Name="Query Logic Trigger CH State LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Logic Trigger CH State LeCroy.vi"/>
			<Item Name="Query Logic Trigger State Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Logic Trigger State Tek.vi"/>
			<Item Name="Query Logic Trigger Threshold Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Logic Trigger Threshold Tek.vi"/>
			<Item Name="Query MaxSamplingRate LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query MaxSamplingRate LeCroy.vi"/>
			<Item Name="Query No of points Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query No of points Agilent.vi"/>
			<Item Name="Query ON-OFF LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query ON-OFF LeCroy.vi"/>
			<Item Name="Query ON-OFF Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query ON-OFF Tek.vi"/>
			<Item Name="Query Output State TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Query VI&apos;s/Query Output State TTi-PL.vi"/>
			<Item Name="Query OVP State.vi" Type="VI" URL="../../../Dependencies/Agilent E364X Series/Public/Action-Status VI&apos;s/Query OVP State.vi"/>
			<Item Name="Query Record Length Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Record Length Tek.vi"/>
			<Item Name="Query Sample Points LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Sample Points LeCroy.vi"/>
			<Item Name="Query Sampling Linits Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Sampling Linits Agilent.vi"/>
			<Item Name="Query Set Voltage TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Query VI&apos;s/Query Set Voltage TTi-PL.vi"/>
			<Item Name="Query Set Voltage.vi" Type="VI" URL="../../../Dependencies/Agilent E364X Series/Public/Action-Status VI&apos;s/Query Set Voltage.vi"/>
			<Item Name="Query Time Offset Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Time Offset Agilent.vi"/>
			<Item Name="Query Time Offset Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Time Offset Tek.vi"/>
			<Item Name="Query Time Reference Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Time Reference Agilent.vi"/>
			<Item Name="Query Time Scale Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Configure/Simple/Query Time Scale Agilent.vi"/>
			<Item Name="Query Time Scale Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Time Scale Tek.vi"/>
			<Item Name="Query TimeBase LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query TimeBase LeCroy.vi"/>
			<Item Name="Query TimeOffset LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query TimeOffset LeCroy.vi"/>
			<Item Name="Query Trigg Class Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Trigg Class Tek.vi"/>
			<Item Name="Query Trigg Type Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Trigger/Query Trigg Type Tek.vi"/>
			<Item Name="Query Trigger CH Coupling LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Trigger CH Coupling LeCroy.vi"/>
			<Item Name="Query Trigger CH Level LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Trigger CH Level LeCroy.vi"/>
			<Item Name="Query Trigger CH Slope LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Trigger CH Slope LeCroy.vi"/>
			<Item Name="Query Trigger Mode.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Trigger Mode.vi"/>
			<Item Name="Query TriggerMode LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query TriggerMode LeCroy.vi"/>
			<Item Name="Query TriggerSelect LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query TriggerSelect LeCroy.vi"/>
			<Item Name="Query Vertical Offset LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Vertical Offset LeCroy.vi"/>
			<Item Name="Query Vertical Offset Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Vertical Offset Tek.vi"/>
			<Item Name="Query Vertical Range LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Configure/Query/Query Vertical Range LeCroy.vi"/>
			<Item Name="Query Vertical Range Tek.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Configure/Query/Query Vertical Range Tek.vi"/>
			<Item Name="Ramp_Down.vi" Type="VI" URL="../../../Dependencies/Keithley 24XX/Public/Ramp_Down.vi"/>
			<Item Name="Range Unit Conversion.vi" Type="VI" URL="../../Oscilloscopes/Range Unit Conversion.vi"/>
			<Item Name="Read Current Limit.vi" Type="VI" URL="../../../Dependencies/Agilent E363X Series/Public/Data/Read Current Limit.vi"/>
			<Item Name="Read Multiple Waveforms to file.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Data/Read Multiple Waveforms to file.vi"/>
			<Item Name="Read Output.vi" Type="VI" URL="../../../Dependencies/Agilent E363X Series/Public/Data/Read Output.vi"/>
			<Item Name="Read Segmented dT LeCroy.vi" Type="VI" URL="../../../Dependencies/LeCroy Wave Series/Public/Read Segmented dT LeCroy.vi"/>
			<Item Name="RefNum Logic Agilent.vi" Type="VI" URL="../../Oscilloscopes/RefNum Logic Agilent.vi"/>
			<Item Name="RefNum Logic LeCoy.vi" Type="VI" URL="../../Oscilloscopes/RefNum Logic LeCoy.vi"/>
			<Item Name="RefNum Logic TekTroniX.vi" Type="VI" URL="../../Oscilloscopes/RefNum Logic TekTroniX.vi"/>
			<Item Name="Request non-cyclic data.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Request non-cyclic data.vi"/>
			<Item Name="Reset Limit Error TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Data VI&apos;s/Reset Limit Error TTi-PL.vi"/>
			<Item Name="Reset TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Utility VI&apos;s/Reset TTi-PL.vi"/>
			<Item Name="RetryOnError.vi" Type="VI" URL="../../../Dependencies/Agilent E363X Series/Public/Utility/RetryOnError.vi"/>
			<Item Name="RetryOnError_Read TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Utility VI&apos;s/RetryOnError_Read TTi-PL.vi"/>
			<Item Name="RetryOnError_Read.vi" Type="VI" URL="../../../Dependencies/Agilent E364X Series/Public/Utility VI&apos;s/RetryOnError_Read.vi"/>
			<Item Name="RetryOnError_Write TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Utility VI&apos;s/RetryOnError_Write TTi-PL.vi"/>
			<Item Name="RetryOnError_Write.vi" Type="VI" URL="../../../Dependencies/Agilent E364X Series/Public/Utility VI&apos;s/RetryOnError_Write.vi"/>
			<Item Name="RetryOnErrorRead.vi" Type="VI" URL="../../../Dependencies/Agilent E363X Series/Public/Utility/RetryOnErrorRead.vi"/>
			<Item Name="Rounding.vi" Type="VI" URL="../Rounding.vi"/>
			<Item Name="Save Multiple Waveforms to File Mod.vi" Type="VI" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Public/Data/Save Multiple Waveforms to File Mod.vi"/>
			<Item Name="Send-Receive Command LAN NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/Send-Receive Command LAN NoAir.vi"/>
			<Item Name="Send-Receive Command LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Send-Receive Command LAN.vi"/>
			<Item Name="Send-Receive Command Serial.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/Send-Receive Command Serial.vi"/>
			<Item Name="Sequence measuremet.vi" Type="VI" URL="../Charged Particle/Sequence measuremet.vi"/>
			<Item Name="Set AllOutput State TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Configure VI&apos;s/Set AllOutput State TTi-PL.vi"/>
			<Item Name="Set Comparator.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Set Comparator.vi"/>
			<Item Name="Set Controller Manual On-Off.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Set Controller Manual On-Off.vi"/>
			<Item Name="Set Current Limit TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Configure VI&apos;s/Set Current Limit TTi-PL.vi"/>
			<Item Name="Set Mode.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Set Mode.vi"/>
			<Item Name="Set On-Off.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Set On-Off.vi"/>
			<Item Name="Set Output State TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Configure VI&apos;s/Set Output State TTi-PL.vi"/>
			<Item Name="Set parameters LAN NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/Set parameters LAN NoAir.vi"/>
			<Item Name="Set parameters LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Set parameters LAN.vi"/>
			<Item Name="Set parameters Serial.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/Set parameters Serial.vi"/>
			<Item Name="Set Temp Init.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Set Temp Init.vi"/>
			<Item Name="Set Temperature.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Set Temperature.vi"/>
			<Item Name="Set Voltage Level TTi-PL.vi" Type="VI" URL="../../../Dependencies/TTi PL330DP/Public/Configure VI&apos;s/Set Voltage Level TTi-PL.vi"/>
			<Item Name="Simple control reference MK115.vi" Type="VI" URL="../../../Dependencies/Binder MK115/Binder MK115.llb/Simple control reference MK115.vi"/>
			<Item Name="Simple control Reference NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/Simple control Reference NoAir.vi"/>
			<Item Name="Simple control Reference vc2020.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/Simple control Reference vc2020.vi"/>
			<Item Name="Simple control reference.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/Simple control reference.vi"/>
			<Item Name="Single Channel 363x.vi" Type="VI" URL="../../Agilent 363x power supply/Single Channel 363x.vi"/>
			<Item Name="Single Channel 364x.vi" Type="VI" URL="../../Agilent 364x power supply/Single Channel 364x.vi"/>
			<Item Name="Single Channel Agilent.vi" Type="VI" URL="../../Oscilloscopes/Single Channel Agilent.vi"/>
			<Item Name="Single Channel hmp4040.vi" Type="VI" URL="../../Hameg HMP4040/Single Channel hmp4040.vi"/>
			<Item Name="Single Channel LeCroy.vi" Type="VI" URL="../../Oscilloscopes/Single Channel LeCroy.vi"/>
			<Item Name="Single Channel TekTorniX.vi" Type="VI" URL="../../Oscilloscopes/Single Channel TekTorniX.vi"/>
			<Item Name="Single Channel TTiPL330.vi" Type="VI" URL="../../TTi PL330DP/Single Channel TTiPL330.vi"/>
			<Item Name="Single Voltage Point.vi" Type="VI" URL="../Charged Particle/Single Voltage Point.vi"/>
			<Item Name="subhmp4000_core_attribute_express.vi" Type="VI" URL="../../../Dependencies/Hameg HMP4040/user.lib/_express/hmp4000/hmp4000_core_attribute_expressSource.llb/subhmp4000_core_attribute_express.vi"/>
			<Item Name="Tektronix 7000 Series.lvlib" Type="Library" URL="../../../Dependencies/Tektronix Scope/Tektronix TDS 7000 Series/Tektronix 7000 Series.lvlib"/>
			<Item Name="Time Unit Conversion.vi" Type="VI" URL="../../Oscilloscopes/Time Unit Conversion.vi"/>
			<Item Name="TimeBase Agilent.vi" Type="VI" URL="../../Oscilloscopes/TimeBase Agilent.vi"/>
			<Item Name="TimeBase Lecory.vi" Type="VI" URL="../../Oscilloscopes/TimeBase Lecory.vi"/>
			<Item Name="TimeBase TekTroniX.vi" Type="VI" URL="../../Oscilloscopes/TimeBase TekTroniX.vi"/>
			<Item Name="TokenizeString.vi" Type="VI" URL="../TokenizeString.vi"/>
			<Item Name="Trigger Edge Agilent.vi" Type="VI" URL="../../Oscilloscopes/Trigger Edge Agilent.vi"/>
			<Item Name="Trigger Edge LeCroy.vi" Type="VI" URL="../../Oscilloscopes/Trigger Edge LeCroy.vi"/>
			<Item Name="Trigger Edge TekTroniX.vi" Type="VI" URL="../../Oscilloscopes/Trigger Edge TekTroniX.vi"/>
			<Item Name="Trigger Logic Agilent.vi" Type="VI" URL="../../Oscilloscopes/Trigger Logic Agilent.vi"/>
			<Item Name="Trigger Logic LeCroy.vi" Type="VI" URL="../../Oscilloscopes/Trigger Logic LeCroy.vi"/>
			<Item Name="Trigger Logic TekTroniX.vi" Type="VI" URL="../../Oscilloscopes/Trigger Logic TekTroniX.vi"/>
			<Item Name="Trigger Rate.vi" Type="VI" URL="../Autotrigger/Trigger Rate.vi"/>
			<Item Name="Ttansmit non-cyclic data.vi" Type="VI" URL="../../../Dependencies/Binder MK53/Binder Oven Control.llb/Ttansmit non-cyclic data.vi"/>
			<Item Name="TTiPL330 RefeNum.vi" Type="VI" URL="../../TTi PL330DP/TTiPL330 RefeNum.vi"/>
			<Item Name="Unit Conversion.vi" Type="VI" URL="../../Hameg HMP4040/Unit Conversion.vi"/>
			<Item Name="View setpoint,temp and on-off LAN NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002 NoAir/Votsch Oven Control.llb/View setpoint,temp and on-off LAN NoAir.vi"/>
			<Item Name="View setpoint,temp and on-off LAN.vi" Type="VI" URL="../../../Dependencies/Votsch VT4002/Votsch Oven Control v2.llb/View setpoint,temp and on-off LAN.vi"/>
			<Item Name="View setpoint,temp and on-off Serial NoAir.vi" Type="VI" URL="../../../Dependencies/Votsch VC2020/Votsch Control VC2020.llb/View setpoint,temp and on-off Serial NoAir.vi"/>
			<Item Name="Voltage Value Logic.vi" Type="VI" URL="../Autotrigger/Voltage Value Logic.vi"/>
			<Item Name="Wait For Operation Complete Ref Agilent.vi" Type="VI" URL="../../../Dependencies/Agilent 8000 Series/Public/Data/Low Level/Wait For Operation Complete Ref Agilent.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Timing Automation &amp; Control" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{CA92B4C6-C2FC-4010-832E-821A5CA718D9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{DC0C459E-CA39-4419-87D9-9D5E55F5F167}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{FC2B0BDD-2A94-48D2-A646-79706A878F54}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Timing Automation &amp; Control</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/egkougko/cernbox/WINDOWS/Desktop/TiCAS/Builds/TiCAS</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{060BDB8A-97F0-44CE-864D-DF0A09F6048B}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">TiCAS.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/egkougko/cernbox/WINDOWS/Desktop/TiCAS/Builds/TiCAS/TiCAS.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/egkougko/cernbox/WINDOWS/Desktop/TiCAS/Builds/TiCAS/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{35B56C97-9F74-47BD-9EC2-C4B8832E2732}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UFSD Control.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">CERN</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Timing Automation &amp; Control</Property>
				<Property Name="TgtF_internalName" Type="Str">Timing Automation &amp; Control</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2021 Vagelis Gkougkousis</Property>
				<Property Name="TgtF_productName" Type="Str">Timing Automation &amp; Control</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{86C7E558-4F38-474D-A364-7AED4D02954D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">TiCAS.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
